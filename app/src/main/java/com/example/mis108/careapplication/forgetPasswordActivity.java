package com.example.mis108.careapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class forgetPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);


        final Button enterVerifyButton = (Button) findViewById(R.id.forgetPasswordSendButton);
        enterVerifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent enterVerifyIntent = new Intent(forgetPasswordActivity.this,enterVerifyActivity.class);
                forgetPasswordActivity.this.startActivity(enterVerifyIntent);
            }
        });
    }


}
