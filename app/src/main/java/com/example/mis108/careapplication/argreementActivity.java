package com.example.mis108.careapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class argreementActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_argreement);


        final Button agreeButton = (Button) findViewById(R.id.agreeButton);
        agreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent agreeIntent = new Intent(argreementActivity.this,registerActivity.class);
                argreementActivity.this.startActivity(agreeIntent);
            }
        });

        final Button DagreeButton = (Button) findViewById(R.id.dontAgreeButton);
        DagreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent DagreeIntent = new Intent(argreementActivity.this,LoginActivity.class);
                argreementActivity.this.startActivity(DagreeIntent);
            }
        });


    }
}
