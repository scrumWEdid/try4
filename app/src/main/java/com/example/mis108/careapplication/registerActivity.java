package com.example.mis108.careapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class registerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        final Button finishRegister = (Button) findViewById(R.id.registerButton);
        finishRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent finishRegisterIntent = new Intent(registerActivity.this,LoginActivity.class);
                registerActivity.this.startActivity(finishRegisterIntent);
            }
        });
    }
}
