package com.example.mis108.careapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class editPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);

        final Button finishEditPw = (Button) findViewById(R.id.sendEditPw);
        finishEditPw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent verifyIntent = new Intent(editPasswordActivity.this,LoginActivity.class);
                editPasswordActivity.this.startActivity(verifyIntent);
            }
        });
    }
}
